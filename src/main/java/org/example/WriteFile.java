package org.example;

import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.OpenAPIV3Parser;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WriteFile {
    private static OpenAPI openAPI;

    public static void main(String[] args) throws IOException {
        Map<String, RequestInfo> scenarios = getDataFromSwagger();
        createDirectory("template");
        writeDataToFileCommon("template", scenarios);
        writeDataToFileSetUpAndAssertion("template", scenarios);
    }

    private static Map<String, RequestInfo> getDataFromSwagger() {
        Map<String, RequestInfo> scenarios = new HashMap<>();
        openAPI = new OpenAPIV3Parser().read("swagger.json");
        openAPI.getPaths().forEach((key, item) -> {
            RequestInfo requestInfo = new RequestInfo(item, key);
            scenarios.put(requestInfo.getScenario(), requestInfo);
        });
        return scenarios;
    }

    private static void writeDataToFileCommon(String directory, Map<String, RequestInfo> scenarios) {
        for (RequestInfo requestInfo : scenarios.values()) {
            writeData(directory, requestInfo.getScenario(), "@ignore", true);
            writeData(directory, requestInfo.getScenario(), "Feature: Skeleton of request", true);
            writeData(directory, requestInfo.getScenario(), "   Background:", true);
            writeData(directory, requestInfo.getScenario(), "     * url baseUrls", true);
            writeData(directory, requestInfo.getScenario(), "   " + requestInfo.getAnnotation(), true);
            writeData(directory, requestInfo.getScenario(), "   Scenario: " + requestInfo.getScenarioName(), true);
            writeData(directory, requestInfo.getScenario(), "   " + requestInfo.getSummary(), true);
            writeData(directory, requestInfo.getScenario(), "      * path " + "\"" + requestInfo.getPath() + "\"", true);
            for (String param : requestInfo.getParameterQueries()) {
                writeData(directory, requestInfo.getScenario(), "      * param " + param + " = " + param, true);
            }
            writeData(directory, requestInfo.getScenario(), "      * method " + requestInfo.getMethod().toUpperCase(Locale.ROOT), true);
            writeData(directory, requestInfo.getScenario(), "      * if (responseStatus != status) karate.log('State of " + requestInfo.getMethod() + " response: ' + responseStatus + ' ' + response)", true);
            writeData(directory, requestInfo.getScenario(), "      * match responseStatus == status", true);
        }
    }

    private static void writeDataToFileSetUpAndAssertion(String directory, Map<String, RequestInfo> scenarios) {
        for (RequestInfo requestInfo : scenarios.values()) {
            writeData(directory, requestInfo.getScenario(), "Feature: " + CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, requestInfo.getTag()), false);
            writeData(directory, requestInfo.getScenario(), "   " + requestInfo.getAnnotation(), false);
            writeData(directory, requestInfo.getScenario(), "   Scenario: " + requestInfo.getScenarioName(), false);
            writeData(directory, requestInfo.getScenario(), "   " + requestInfo.getSummary(), false);
            writeData(directory, requestInfo.getScenario(), "      * table RequestParam", false);
            writeData(directory, requestInfo.getScenario(), "         | Sr.No | Scenario|" + requestInfo.getTableParamHeader() + " status |", false);
            writeData(directory, requestInfo.getScenario(), "         | 1 | '" + (requestInfo.getSummary().length() < 50 ? requestInfo.getSummary() : StringUtils.left(requestInfo.getSummary(), 50) + "...") + "'|  " + requestInfo.getTableParamType() + "200|", false);
            writeData(directory, requestInfo.getScenario(), "      * def jsonResponse =  call read('classpath:api/" + requestInfo.getTag() + "/common/" + requestInfo.getScenario() + "Common.feature" + requestInfo.getAnnotation() + "') RequestParam", false);
            writeData(directory, requestInfo.getScenario(), "      * karate.log(jsonResponse)", false);
        }
    }

    private static void writeData(String directory, String fileName, String data, Boolean isCommonActionFile) {
        try {
            String file = isCommonActionFile ? fileName + "Common.feature" : fileName + ".feature";
            FileWriter myWriter = new FileWriter(directory + "/" + file, true);
            myWriter.write(data + System.lineSeparator());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createDirectory(String templateFolder) {
        File template = new File(templateFolder);
        if (!template.exists()) {
            template.mkdir();
        }
    }
}

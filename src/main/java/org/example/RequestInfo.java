package org.example;

import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.parameters.Parameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RequestInfo {
    private String method;
    private String endpoint;
    private String summary;
    private String tag;
    private String path;
    private String scenario;
    private String scenarioName;
    private String annotation;
    private String tableParamHeader;
    private String tableParamType;
    private List<String> parameterQueries;

    public RequestInfo(PathItem item, String path) {
        this.method = getMethod(item);
        this.endpoint = path;
        this.summary = getSummary(item);
        this.tag = getTag(item);
        this.path = path.replaceAll("\\{", "\" + ").replaceAll("\\}", " + \"");
        this.scenario = method + CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, path.replaceAll("[^a-zA-Z0-9]", "-"));
        this.scenarioName = method.toUpperCase(Locale.ROOT) + " " + path;
        this.annotation = "@" + scenario;
        this.tableParamHeader = getParameterNames(item);
        this.tableParamType = getParameterTypes(item);
        this.parameterQueries = getParameterQueries(item);
    }

    public String getMethod() {
        return method;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getSummary() {
        return summary;
    }

    public String getTag() {
        return tag;
    }

    public String getPath() {
        return path;
    }

    public String getScenario() {
        return scenario;
    }

    public String getScenarioName() {
        return scenarioName;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String getTableParamHeader() {
        return tableParamHeader;
    }

    public String getTableParamType() {
        return tableParamType;
    }

    public List<String> getParameterQueries() {
        return parameterQueries;
    }

    private String getMethod(PathItem item) {
        return getOperations(item).stream()
                .map(Operation::getOperationId)
                .findFirst()
                .orElse("");
    }

    private String getSummary(PathItem item) {
        return getOperations(item).stream()
                .map(Operation::getSummary)
                .findFirst()
                .orElse("");
    }

    private String getTag(PathItem item) {
        return getOperations(item).stream()
                .flatMap(op -> op.getTags().stream())
                .findFirst()
                .map(tag -> tag.replaceAll("-", "").replaceAll("controller", ""))
                .orElse("");
    }

    private String getParameterNames(PathItem item) {
        StringBuilder sb = new StringBuilder();
        getOperations(item).forEach(op -> {
            if (op.getParameters() != null) {
                for (Parameter p : op.getParameters()) {
                    sb.append(p.getName()).append(" | ");
                }
            }
        });
        return sb.toString();
    }

    private String getParameterTypes(PathItem item) {
        StringBuilder sb = new StringBuilder();
        getOperations(item).forEach(op -> {
            if (op.getParameters() != null) {
                for (Parameter p : op.getParameters()) {
                    sb.append(p.getSchema().getType()).append(" | ");
                }
            }
        });
        return sb.toString();
    }

    private List<String> getParameterQueries(PathItem item) {
        List<String> parameterQueries = new ArrayList<>();
        getOperations(item).forEach(op -> {
            if (op.getParameters() != null) {
                for (Parameter p : op.getParameters()) {
                    if ("query".equalsIgnoreCase(p.getIn())) {
                        parameterQueries.add(p.getName());
                    }
                }
            }
        });
        return parameterQueries;
    }

    private List<Operation> getOperations(PathItem item) {
        List<Operation> operations = new ArrayList<>();
        if (item.getHead() != null) operations.add(item.getHead());
        if (item.getGet() != null) operations.add(item.getGet());
        if (item.getPost() != null) operations.add(item.getPost());
        if (item.getPatch() != null) operations.add(item.getPatch());
        if (item.getPut() != null) operations.add(item.getPut());
        if (item.getDelete() != null) operations.add(item.getDelete());
        return operations;
    }
}

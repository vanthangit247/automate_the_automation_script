@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetPetFindbystatus
   Scenario: GET /pet/findByStatus
   Finds Pets by status
      * path "/pet/findByStatus"
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

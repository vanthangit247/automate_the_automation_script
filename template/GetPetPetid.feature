Feature:  Pet
   @GetPetPetid
   Scenario: GET /pet/{petId}
   Find pet by ID
      * table RequestParam
         | Sr.No | Scenario|petId |  status |
         | 1 | 'Find pet by ID'|  integer | 200|
      * def jsonResponse =  call read('classpath:api/pet/common/GetPetPetidCommon.feature@GetPetPetid') RequestParam
      * karate.log(jsonResponse)

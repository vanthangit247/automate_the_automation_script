@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetPetFindbytags
   Scenario: GET /pet/findByTags
   Finds Pets by tags
      * path "/pet/findByTags"
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

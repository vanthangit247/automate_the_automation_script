@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetStoreInventory
   Scenario: GET /store/inventory
   Returns pet inventories by status
      * path "/store/inventory"
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

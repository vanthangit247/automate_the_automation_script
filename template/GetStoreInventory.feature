Feature:  Store
   @GetStoreInventory
   Scenario: GET /store/inventory
   Returns pet inventories by status
      * table RequestParam
         | Sr.No | Scenario| status |
         | 1 | 'Returns pet inventories by status'|  200|
      * def jsonResponse =  call read('classpath:api/store/common/GetStoreInventoryCommon.feature@GetStoreInventory') RequestParam
      * karate.log(jsonResponse)

@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetUserUsername
   Scenario: GET /user/{username}
   Get user by user name
      * path "/user/" + username + ""
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

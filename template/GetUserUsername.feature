Feature:  User
   @GetUserUsername
   Scenario: GET /user/{username}
   Get user by user name
      * table RequestParam
         | Sr.No | Scenario|username |  status |
         | 1 | 'Get user by user name'|  string | 200|
      * def jsonResponse =  call read('classpath:api/user/common/GetUserUsernameCommon.feature@GetUserUsername') RequestParam
      * karate.log(jsonResponse)

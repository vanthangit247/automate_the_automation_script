@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetUserLogin
   Scenario: GET /user/login
   Logs user into the system
      * path "/user/login"
      * param username = username
      * param  password =  password
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

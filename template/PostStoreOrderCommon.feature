@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @PostStoreOrder
   Scenario: POST /store/order
   Place an order for a pet
      * path "/store/order"
      * method POST
      * if (responseStatus != status) karate.log('State of Post response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetUserLogout
   Scenario: GET /user/logout
   Logs out current logged in user session
      * path "/user/logout"
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @PostPet
   Scenario: POST /pet
   Add a new pet to the store
      * path "/pet"
      * method POST
      * if (responseStatus != status) karate.log('State of Post response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

Feature:  Store
   @GetStoreOrderOrderid
   Scenario: GET /store/order/{orderId}
   Find purchase order by ID
      * table RequestParam
         | Sr.No | Scenario|orderId |  status |
         | 1 | 'Find purchase order by ID'|  integer | 200|
      * def jsonResponse =  call read('classpath:api/store/common/GetStoreOrderOrderidCommon.feature@GetStoreOrderOrderid') RequestParam
      * karate.log(jsonResponse)

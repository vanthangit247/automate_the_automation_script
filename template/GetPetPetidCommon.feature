@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetPetPetid
   Scenario: GET /pet/{petId}
   Find pet by ID
      * path "/pet/" + petId + ""
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status

Feature:  User
   @GetUserLogout
   Scenario: GET /user/logout
   Logs out current logged in user session
      * table RequestParam
         | Sr.No | Scenario| status |
         | 1 | 'Logs out current logged in user session'|  200|
      * def jsonResponse =  call read('classpath:api/user/common/GetUserLogoutCommon.feature@GetUserLogout') RequestParam
      * karate.log(jsonResponse)

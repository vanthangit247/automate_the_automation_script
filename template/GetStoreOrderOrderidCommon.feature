@ignore
Feature: Skeleton of request
   Background:
     * url baseUrls
   @GetStoreOrderOrderid
   Scenario: GET /store/order/{orderId}
   Find purchase order by ID
      * path "/store/order/" + orderId + ""
      * method GET
      * if (responseStatus != status) karate.log('State of Get response: ' + responseStatus + ' ' + response)
      * match responseStatus == status
